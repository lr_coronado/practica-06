﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    public class Persona 
    {
        private string cedula;
        private string nombre;
        private string apellido;
        private byte edad;

        public string Cedula 
        {
            get 
            {
                return this.cedula;
            }
            set 
            {
                this.cedula = value;
            }
        }

        public string Nombre 
        {
            get 
            {
                return this.nombre;
            }
            set 
            {
                this.nombre = value;
            }
        }

        public string Apellido 
        {
            get 
            {
                return this.apellido;
            }
            set 
            {
                this.apellido = value;
            }
        }

        public byte Edad 
        {
            get 
            {
                return this.edad;
            }
            set 
            {
                if (value > 200)
                {
                    this.edad = 0;
                }
                else 
                {
                    this.edad = value;
                }
            }
        }

        public Persona() { }

        public Persona(string cedula, string nombre,string apellido, byte edad) 
        {
            this.Cedula = cedula;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Edad = edad;
        }

        public void Responsabilidad() 
        {
            Console.WriteLine("Soy una Persona.");
        }

        public void ImprimirDatosDePersona() 
        {
            Console.WriteLine("Cedula: " + this.Cedula);
            Console.WriteLine("Nombre: " + this.Nombre);
            Console.WriteLine("Apellido: " + this.Apellido);
            Console.WriteLine("Edad: " + this.Edad);
        }
    }

    public class Profesor : Persona
    {
        public decimal sueldo;
        public decimal Sueldo 
        {
            get { return this.sueldo; }
            set 
            {
                if (value.GetType() != typeof(decimal))
                {
                    this.sueldo = 0;
                }
                else 
                {
                    this.sueldo = value;
                }
            }
        }

        public Profesor(decimal sueldo)
        {
            this.Sueldo = sueldo;
        }

        public void ImprimirSueldo() 
        {
            Console.WriteLine("El Sueldo del Profesor es: " + this.Sueldo);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Persona nuevaPersona = new Persona("402-1558982-6", "Luis", "Coronado", 18);
            nuevaPersona.Responsabilidad();
            nuevaPersona.ImprimirDatosDePersona();

            Console.WriteLine("\n");

            Profesor nuevoProfesor = new Profesor(25000)
            {
                Cedula = "001-2589879-8",
                Nombre = "Miguel",
                Apellido = "Moreta",
                Edad = 0
            };

            Console.WriteLine("\n");

            nuevoProfesor.ImprimirDatosDePersona();
            nuevoProfesor.ImprimirSueldo();
        }
    }
}
