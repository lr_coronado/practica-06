﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    public class Contacto 
    {
        public string nombre;
        public string apellido;
        public string telefono;
        public string direccion;

        public void SetContacto(string nombre, string apellido, string telefono, string direccion) 
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.telefono = telefono;
            this.direccion = direccion;
        }

        public void ImprimirContacto() 
        {
            Console.WriteLine("Nombre: " + this.nombre);
            Console.WriteLine("Apellido: " + this.apellido);
            Console.WriteLine("Telefono: " + this.telefono);
            Console.WriteLine("Direccion: " + this.direccion);
        }

        public void Saludar() 
        {
            Console.WriteLine($"Hola Soy {this.nombre} {this.apellido}. Mis Datos Son: ");
            Console.WriteLine("Telefono: " + this.telefono);
            Console.WriteLine("Direccion: " + this.direccion);
        }
    }

    class ProbarContacto
    {
        static void Main(string[] args)
        {
            Contacto nuevoContacto = new Contacto() 
            {
                nombre = "Luis",
                apellido = "Coronado",
                telefono = "879-878-9878",
                direccion = "Los Coquitos, Calle #4"
            };

            nuevoContacto.Saludar();

            Console.WriteLine("\n");

            Contacto nuevoContacto2 = new Contacto()
            {
                nombre = "Luis",
                apellido = "Coronado",
                telefono = "879-878-9878",
                direccion = "Los Coquitos, Calle #4"
            };

            nuevoContacto2.Saludar();

        }
    }
}
