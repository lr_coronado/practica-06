﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    public class ClaseA 
    {
        public ClaseA() 
        {
            Console.WriteLine("Soy la Clase A");
        }
    }
    public class ClaseB : ClaseA
    {
        public ClaseB()
        {
            Console.WriteLine("Soy la Clase B");
        }
    }
    public class ClaseC : ClaseB
    {
        public ClaseC()
        {
            Console.WriteLine("Soy la Clase C");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ClaseC objeto = new ClaseC();
        }
    }
}
